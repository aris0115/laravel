<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('home');
    }
    public function table(){
        return view('table');
    }
    public function datatables(){
        return view('datatables');
    }
}
