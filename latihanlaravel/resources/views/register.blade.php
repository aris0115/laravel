@extends('layout.master')

@section('judul')
Form Register
@endsection

@section('isi')
<form action='/welcome' method="POST">
    @csrf
    <h1 align="center">Buat Account Baru!</h1>
    <fieldset>
        <legend align="center"><h3>Sign Up Form</h3></legend>
        <table align="center">
            <tr><td><label>First Name</label><td> <input type="text" name="fn"></td></tr><br>
            <tr><td><label>Last Name</label></td><td><input type="text" name="ln"></td></tr><br>
            <tr><td><label>Date of Birth</label></td><td><input type="date" name="date"></td></tr><br>

            <tr><td><label>Gender</label></td><td><input type="radio" name="gender" value="0"><label>Male</label></td></tr>
                <tr><td></td><td><input type="radio" name="gender" value="1"><label>Female</label></td></tr>
                <tr><td></td> <td> <input type="radio" name="gender" value="2"><label>Other</label></td></tr>

            <tr>
                <td><label>Nationality</label></td>
                <td>
                    <select name="nation">
                        <option value="ina">Indonesian</option>
                        <option value="ind">India</option>
                        <option value="wna">Other Nation</option>
                    </select>
                </td>
            </tr>
            <tr><td><label>Language Spoken :</label></td><td><input type="checkbox" name="language" value="bi"><label>Bahasa Indonesia</label></td></tr>
                <tr><td></td><td><input type="checkbox" name="language" value="eng"><label>English</label></td></tr>
                <tr><td></td><td><input type="checkbox" name="language" value="other"><label>Other</label></td></tr>
            
            <tr><td><label>Bio</label></td><td><textarea name="bio" id="" cols="30" rows="10"></textarea></td></tr>
        </table>
    </fieldset>
    <input type="submit" name="signup" value="Sign Up"><br>
</form>

@endsection
    
