<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function selamat(Request $request){
        $firstname = $request->fn;
        $lastname = $request->ln;
        $date = $request->date;
        $gender = $request->gender;
        $nation = $request->nation;
        $language = $request->language;
        $bio = $request->bio;
        return view('welcomee',compact('firstname','lastname','date','gender','nation','language','bio'));
    }
}
